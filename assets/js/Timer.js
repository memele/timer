export default class Timer {
  constructor(element) {
    this.timerElement = element;
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
    this.started = false;

    this.toggleButton = this.timerElement.querySelector('.toggle');
    this.toggleIcon = this.toggleButton.querySelector('img');
    this.time = this.timerElement.querySelector('.time');
    this.expandIcon = this.timerElement.querySelector('.maximize img');

    this.init();
  }

  init = () => {
    this.toggleButton.addEventListener('click', () => {
      this.toggleTimer();
    });
  }

  toggleTimer = () => {
    this.started = !this.started;
    this.updateIcons();

    if (this.started) {
      this.startTimer();
    } else {
      this.stopTimer();
    }
  }

  updateIcons = () => {
    let toggleIcon = this.started ? 'stop' : 'play';
    let expandIcon = this.started ? 'maximize-disabled' : 'maximize-default';

    this.toggleIcon.src = `assets/icons/${toggleIcon}.svg`;
    this.expandIcon.src = `assets/icons/${expandIcon}.svg`;
  }

  startTimer = () => {
    this.timer = setInterval(this.timerCycle, 1000);
  }

  timerCycle = () => {
    this.hours = parseInt(this.hours);
    this.minutes = parseInt(this.minutes);
    this.seconds = parseInt(this.seconds);

    // Update time units
    this.seconds = this.seconds + 1;
    if (this.seconds === 60) {
      this.minutes = this.minutes + 1;
      this.seconds = 0;
    }

    if (this.minutes === 60) {
      this.hours = this.hours + 1;
      this.minutes = 0;
      this.seconds = 0;
    }

    // Pad time units with a leading zero
    let units = ['seconds', 'minutes', 'hours'];
    units.forEach((element) => {
      this[element] = String(this[element]).padStart(2, '0');
    });

    this.time.innerHTML = `${this.hours}:${this.minutes}:${this.seconds}`;
  }

  stopTimer = () => {
    window.clearInterval(this.timer);

    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
    this.time.innerHTML = '00:00:00';
  }
}
